
## GitBasics 
#### Branch  
First thing to do before making any modification to the tree (Repo) or the master branch is to make a branch of it.
#### Clone  
Make a duplicate of the repo in the local machine. Note that here there are no changes made to the name of the document as well. 
#### Fork  
To make a new repo in the name of username rather than just duplicating it like clone
#### Commit  
As one is ready with their own version of a code, commit saves their work as such in the local machine.
#### Push  
Once  modifications are done  and commited it is pushed to the remote server using this function.  
#### GitInternals  
The below picture  shows the Git Internals.  
1. Workspace:your local editor where modifications are done.
2. Staging: Files that are ready to commit goes here.
3. Local repo: Commited files are saved here.
4. Remote repo: Files are moved here after push command.  

![workflow](https://gitlab.com/iotiotdotin/project-internship/orientation/-/wikis/extras/Git.png)